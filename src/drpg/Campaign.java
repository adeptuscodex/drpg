/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;
import java.util.ArrayList;


/**
 *
 * @author Adeptus Codex
 */
public class Campaign implements java.io.Serializable{
    
   private ArrayList<Map> mapList;
   private ArrayList<Player> playerList;
   private String name;
   private String desc;
   private static long uniqueTokenID;
   private static int uniqueMapID;

   protected Campaign(){
      this("", "");
   }

   public Campaign(String name, String desc){
      this.name = name;
      this.desc = desc;
      mapList = new ArrayList();
      playerList = new ArrayList();
      uniqueTokenID = 0;
      uniqueMapID = 0;
   }
   
   public String getCampaignName(){
       return name;
   }
   
   public void setCampaignName(String input){
       name = input;
   }
   
   public String getDescription(){
       return desc;
   }
   
   public void setDescription(String input){
       desc = input;
   }

   public void setMapList(ArrayList<Map> a){
      mapList = a;
   }

   public ArrayList<Map> getMapList(){
      return mapList;
   }

   public ArrayList<String> getPlayerNames(){
      ArrayList<String> a = new ArrayList();
      for(Player p : playerList){
         a.add(p.getCharName());
      }
      return a;
   }

   public void setPlayerList(ArrayList<Player> a){
      playerList = a;
   }

   public ArrayList<Player> getPlayerList(){
      return playerList;
   }

   public void addPlayer(Player input){
      boolean add = true;
      for(Player p : playerList){
         if (input.equals(p)){
            add = false;
         }
      }
      if(add) {
         playerList.add(input);
      }
   }
    
   public void removePlayer(Player input){
       playerList.remove(input);
   }

   public void addMap(Map m){
      mapList.add(m);
   }

   public void deleteMap(Map m){
      mapList.remove(m);
   }

   //should only be called when opening a saved campaign
   public void setTokenID(long l){
      uniqueTokenID = l;
   }

   public long getNextTokenID(){
       return uniqueTokenID++;
   } 
   
   public void deleteCampaign(Campaign input){
   }

   //should only be called when opening a saved campaign
   public void setMapID(int i){
      uniqueMapID = i;
   }

   public int getNextMapID(){
       return uniqueMapID++;
   }
}
