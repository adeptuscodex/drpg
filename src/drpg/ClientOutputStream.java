/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package drpg;

import java.io.*;

/**
 *
 * @author Matt
 */
public class ClientOutputStream extends ObjectOutputStream
{
   private int streamID;

   protected ClientOutputStream() throws IOException{
      this(null);
   }

   protected ClientOutputStream(OutputStream s) throws IOException{
      this(s, 0);
   }

   public ClientOutputStream(OutputStream s, int id) throws IOException{
      super(s);
      this.streamID = id;
   }

   public void setID(int id){
      streamID = id;
   }

   public int getID(){
      return streamID;
   }
}
