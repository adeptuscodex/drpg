/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;

/**
 *
 * @author Matt
 */
public interface DRPGInterface {

   Player getPlayer();

   void receiveMessage(String msg);
   
   void addMap(Map m);
   
   void addToken(Token t);
   
   void updateMap(TokenXY t);//mapupdate object with tokenxy

   void updateCampaign(Campaign c);

   void addPlayer(Player p);
}
