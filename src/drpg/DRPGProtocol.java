package drpg;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matt
 */
public class DRPGProtocol
{
   private String address;
   private int port;
   private String password;
   private DRPGInterface di;
   
   //as a server
   private boolean hosting;
   private Thread listener;
   private ServerSocket serverSoc;
   private ArrayList<ClientOutputStream> toClients;
   private ObjectInputStream tempInStream;
   private ArrayList<Thread> clientListeners;
   private int cliID;
   
   //as a client
   private ObjectInputStream clientInput;
   private ObjectOutputStream clientOutput;
   private Socket client;
   private boolean clientConnected;
   private Thread clientThread;
   private int myID;

   public DRPGProtocol(DRPGInterface di){
      this.di = di;
      toClients = new ArrayList();
      clientListeners = new ArrayList();
      cliID = 0;
   }

   public void exit(){
      if(clientConnected){
         try {
            clientInput.close();
            clientOutput.close();
            client.close();
            clientConnected = false;
         } catch (IOException e) {}
      }
      else if(hosting){
         for(ObjectOutputStream os : toClients){
            try {
               os.writeObject(new Boolean(true));
               os.flush();
            }
            catch (IOException e) {}
         }
         hosting = false;
         //clear client threads
      }
   }
   
   public void sendObject(Object o){
      System.out.println("Sending an object");
      //client sending
      if(clientConnected){
         System.out.println("sending from client");
         try {
            clientOutput.writeInt(myID);
            clientOutput.writeObject(o);
            clientOutput.flush();
            System.out.println("sent from client");
         }
         catch (IOException e) {}
      }
      //server sending
      else if(hosting){
         System.out.println("sending from server");
         //send object to each connected client
         for(ObjectOutputStream os : toClients){
            System.out.println("sending to a client");
            try {
               os.writeObject(o);
               os.flush();
               System.out.println("sent to a client");
            }
            catch (IOException e) {}
         }
      }
   }

   //try to join a host
   public void joinSession(String ip, int port, String password){
      try {
         System.out.println("trying to join");
         client = new Socket(ip, port);
         System.out.println("made socket");
         clientOutput = new ObjectOutputStream(client.getOutputStream());
         System.out.println("made outstream");
         clientOutput.writeUTF(ip);//write user name
         clientOutput.flush();
         System.out.println("writing string");
         clientInput = new ObjectInputStream(client.getInputStream());
         System.out.println("made instream");
         myID = clientInput.readInt();
         System.out.println("got ID "+myID);
         boolean p = clientInput.readBoolean();
         System.out.println("checking for password");
         if(p){
            clientOutput.writeUTF(password);
            clientOutput.flush();
            boolean goodPass = clientInput.readBoolean();
            if(!goodPass){
               clientOutput.close();
               clientInput.close();
               client.close();
               clientConnected = false;
               return;
            }
         }
         clientConnected = true;
         startClientThread();
         System.out.println("joined");
      }
      catch(Exception e) {}
   }

   //start general listening
   private void startClientThread(){
      clientThread = new Thread(){
         @Override public void run() {
            while(clientConnected){//connected
               try{
                  parseObject(clientInput.readObject());
               }
               catch (IOException | ClassNotFoundException e){}
            }
            try{
               clientInput.close();
               clientOutput.close();
               client.close();
               clientConnected = false;
            }
            catch (IOException e){}
         }
      };
      clientThread.start();
   }

   //start server listening to a single client in a new thread
   private void startServerThread(ObjectInputStream ois){
      tempInStream = ois;//may need locks here
      Thread t = new Thread(){
         @Override public void run() {
            ObjectInputStream i = tempInStream;//and unlock here
            while(hosting){
               try{
                  int senderID = i.readInt();
                  Object o = i.readObject();
                  for(ClientOutputStream out : toClients){
                     if(out.getID() != senderID){
                        out.writeObject(o);
                        out.flush();
                     }
                  }
                  parseObject(o);
               }
               catch (IOException | ClassNotFoundException e){}
            }
         }
      };
      clientListeners.add(t);
      t.start();
   }

   //start hosting and listening for clients to connect
   public void hostSession(int port, String password) {
      setHosting(true);
      setPassword(password);
      setPort(port);
      listener = new Thread(){
         @Override public void run() {
            try {
               serverSoc = new ServerSocket(getPort());
               while(isHosting()) {
                  //listen for new connections
                  Socket newClient = serverSoc.accept();
                  ObjectInputStream ois = new ObjectInputStream(newClient.getInputStream());
                  String s = ois.readUTF();//read user name
                  int newID = cliID++;
                  ClientOutputStream oos = new ClientOutputStream(newClient.getOutputStream(), newID);
                  oos.writeInt(newID);
                  oos.flush();
                  boolean p = hasPassword();
                  oos.writeBoolean(p);
                  oos.flush();
                  if(p){
                     String clientPassword = ois.readUTF();
                     if(!clientPassword.equals(getPassword())){
                        //bad password
                        oos.writeBoolean(false);
                        oos.flush();
                        newClient.close();
                        ois.close();
                        oos.close();
                     }
                  }
                  toClients.add(oos);
                  startServerThread(ois);
                  //DRPGServerThread t = new DRPGServerThread(newClient);
                  //clients.add(t);
                  //t.start();
               }
            }
            catch (IOException e) {}
         }
      };
      listener.start();
   }

   //deal with a read object
   public void parseObject(Object o){
      System.out.println("Parsing an object");
      //messages and private messages
      if(o instanceof Message){
         System.out.println("Parsing: message");
         Message msg = (Message)o;
         //only send to the target
         if(msg.isPrivateMessage()){
            if(di.getPlayer().equals(msg.getTarget())){
               di.receiveMessage(msg.getMessage());
            }
         }
         //don't send the message to the sender
         else{
            if(!di.getPlayer().equals(msg.getSender())) {
               di.receiveMessage(msg.getMessage());
            }
         }
      }
      else if(o instanceof Map){
         System.out.println("Parsing: map");
         di.addMap((Map)o);
      }
      else if(o instanceof TokenXY){
         System.out.println("Parsing: tokenxy");
         di.updateMap((TokenXY)o);
      }
      else if(o instanceof Token){
         System.out.println("Parsing: token");
         di.addToken((Token)o);
      }
      else if(o instanceof Player){
         System.out.println("Parsing: player");
         di.addPlayer((Player)o);
      }
      else if(o instanceof Campaign){
         System.out.println("Parsing: campaign");
         di.updateCampaign((Campaign)o);
      }

      if(clientConnected){
         //use this as a flag to close the connection
         if(o instanceof Boolean){
            clientConnected = false;
            try {
               clientOutput.close();
               clientInput.close();
               client.close();
            }
            catch (IOException e) {}
         }
      }
   }

   public void setAddress(String s) {
      this.address = s;
   }

   public String getAddress() {
      return address;
   }

   public void setPort(int i) {
      this.port = i;
   }

   public int getPort() {
      return port;
   }

   public void setHosting(boolean b) {
      this.hosting = b;
   }

   public boolean isHosting() {
      return hosting;
   }

   public void setPassword(String s) {
      this.password = s;
   }

   public String getPassword() {
      return password;
   }

   public boolean hasPassword() {
      String s = getPassword();
      if(s == null || s.isEmpty()){
         return false;
      }
      return true;
   }
}