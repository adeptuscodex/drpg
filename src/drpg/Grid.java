/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;
import java.awt.*;
import java.awt.image.*;

/**
 *
 * @author Adeptus Codex
 */
public class Grid implements java.io.Serializable {
    
    public static final int GRID_NONE = 0;
    public static final int GRID_SQUARE = 1;
    public static final int GRID_HEX_HORIZONTAL = 2;
    public static final int GRID_HEX_VERTICAL = 3;
    
    private Image gridBase; //one grid unit (one square or one hex)
    private int gridType;
    private int gridSize;
    //width and height should be the same value
    
    public Grid(){
        this(Grid.GRID_NONE, null);
    }    
    
    public Grid(int type, Image gridUnit){
        this.gridType = type;
        this.gridBase = gridUnit;
        this.gridSize = gridUnit.getHeight(null);
    }

    public Image getFullGrid(int x, int y){
       BufferedImage img = new BufferedImage(x, y, BufferedImage.TYPE_INT_ARGB);
       Graphics2D g2 = img.createGraphics();
       g2.setColor(Color.black);

       if(gridType == Grid.GRID_NONE){
          return null;
       }
       gridBase = Grid.drawGridUnit(gridType, gridSize);
       if(gridType == Grid.GRID_SQUARE){
          for(int i=0; i<y; i+=gridSize-1){
             for(int j=0; j<x; j+=gridSize-1){
                g2.drawImage(gridBase, j, i, null);
             }
          }
       }
       else{
          int mid = gridSize/2;
          int r = 1;
          int xx = (int)(mid+(mid)*Math.cos(r*2*Math.PI/6));
          int yy = (int)(mid+(mid)*Math.sin(r*2*Math.PI/6));
          if(gridType == Grid.GRID_HEX_HORIZONTAL){
             for(int i=0; i<y; i+=gridSize-1){
               for(int j=0; j<x; j+=gridSize){
                  if(j%2==0)
                     g2.drawImage(gridBase, j, i, null);
                  else
                     g2.drawImage(gridBase, j+mid, i, null);
               }
            }
          }
       }

       return img;
    }

    /*public void setGridBase(Image input){
        gridBase = input;
    }*/
    
    public Image getGridBase(){
        return gridBase;
    }
    
    /*public void setGridType(int input){
        gridType = input;
    }*/
    
    public int getGridType(){
        return gridType;
    }
    
    public void setGridSize(int i){
        gridSize = i;
    }
    
    public int getGridSize(){
        return gridSize;
    }
    
    public static Image drawGridUnit(int gridType, int size){
        if(gridType == Grid.GRID_NONE) {
            return null;
        }
        if(size<1){
           return null;
        }
        BufferedImage img = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img.createGraphics();
        g2.setColor(Color.black);
        
        if(gridType == Grid.GRID_SQUARE) {
            g2.drawRect(0, 0, size-1, size-1);
        }
        else {
            //todo: get the proper hex for the types
            Polygon p = new Polygon();
            for(int i=0; i<6; i++){
                int mid = size/2;
                int x = (int)(mid+(size/2)*Math.cos(i*2*Math.PI/6));
                int y = (int)(mid+(size/2)*Math.sin(i*2*Math.PI/6));
                if(gridType == Grid.GRID_HEX_VERTICAL){
                    p.addPoint(x, y);
                }
                else if(gridType == Grid.GRID_HEX_HORIZONTAL){
                    p.addPoint(y, x);
                }
            }
            g2.drawPolygon(p);
        }
        
        return img;
    }
}
