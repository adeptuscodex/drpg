/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Adeptus Codex
 */
public class Map extends JPanel implements java.io.Serializable{
    
    private String name;
    private boolean onlyGM;
    private ImageIcon background;
    private ImageIcon[] drawLayers;
    private Grid myGrid;
    private int mapID;
    private boolean snapToGrid;
    private String desc;
    private ArrayList<TokenXY> tokenCoords;

    public Map(Image mapImage, String name, String desc, Grid g){
       background = new ImageIcon(mapImage);
       this.name = name;
       this.desc = desc;
       myGrid = g;
       mapID = DRPG.currentCampaign.getNextMapID();
       drawLayers = new ImageIcon[5];
       tokenCoords = new ArrayList();
       onlyGM = false;
       super.setPreferredSize(new Dimension(mapImage.getWidth(null), mapImage.getHeight(null)));
       super.setLayout(null);
    }

    @Override
    public void paintComponent(Graphics g){
       super.paintComponent(g);
       g.drawImage(background.getImage(), 0, 0, this);
       g.drawImage(myGrid.getFullGrid(this.getPreferredSize().width, this.getPreferredSize().height),
               0, 0, this);
    }

    public String getMapName(){
        return name;
    }
    
    public void setMapName(String input){
        name = input;
    }
    
    public String getMapDescription(){
        return desc;
    }
    
    public void setMapDescription(String input){
        desc = input;
    }
    
    public TokenXY addToken(Token t, int inputx, int inputy){
       //ImageIcon i = new ImageIcon(t.getImageIcon().
       TokenXY txy = new TokenXY(t.getImageIcon(), t.getTokenID(), this);
       txy.addMouseListener(new MouseInputAdapter(){
          int startx, starty;

          @Override
          public void mousePressed(MouseEvent e){
             startx = e.getX();
             starty = e.getY();
          }

          @Override
          public void mouseReleased(MouseEvent e){
             int x = e.getX();
             int y = e.getY();
             TokenXY l = (TokenXY)e.getComponent();
             l.setLocation(l.getX()+(x-startx), l.getY()+(y-starty));
             DRPG.dp.sendObject(l);
          }

          /*@Override
          public void mouseDragged(MouseEvent e){
             JLabel l = (JLabel)e.getComponent();
             l.setLocation(e.getX(), e.getY());
             l.getParent().repaint();
          }*/
       });
       txy.setBounds(inputx, inputy, txy.getIcon().getIconWidth(), txy.getIcon().getIconHeight());
       add(txy);
       repaint();
       DRPG.dp.sendObject(txy);
       return txy;
    }

    public void updateTokenXY(TokenXY t){
       Component[] comps = super.getComponents();
       boolean add = true;
       /*for(Component c : comps){
          TokenXY temp = (TokenXY)c;
          if(t.equals(c)){
          //if(temp.getTokenID() == t.getTokenID()){
             //temp.setLocation(t.getLocation());
             temp.setBounds(t.getBounds());
             repaint();
             add = false;
          }
       }*/
       if(add){
          try{
            remove(t);
          }
          catch (Exception e){}
          System.out.println("updating tokenxy on a map");
          //t.setBounds(t.getX(), t.getY(), t.getIcon().getIconWidth(), t.getIcon().getIconHeight());
          t.setBounds(t.getBounds());
          add(t);
          repaint();
       }
    }
    
    public void moveToken(TokenXY inputtokenxy, int inputx, int inputy){
        if(tokenCoords.indexOf(inputtokenxy) >= 0){
            tokenCoords.set(tokenCoords.indexOf(inputtokenxy), inputtokenxy);
        }
    }
    
    public void setGMOnly(boolean input){
        onlyGM = input;
    }
    
    public void setBackgroundImage(ImageIcon input){
        background = input;
    }
    
    public ImageIcon getBackgroundImage(){
        return background;
    }
    
    public int getMapID(){
        return mapID;
    }
}
