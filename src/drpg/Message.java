/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;

/**
 *
 * @author fatoram5006
 */
public class Message implements java.io.Serializable{
   private Player sender;
   private String message;
   private Player target;
   private boolean privateMessage;
   
   public Message(String message, Player sender){
      this.message = message;
      this.sender = sender;
      this.target = null;
      this.privateMessage = false;
   }
   
   public Message(String message, Player sender, Player target){
      this.message = message;
      this.sender = sender;
      this.target = target;
      this.privateMessage = true;
   }
   
   public String getMessage(){
      return message;
   }
   
   public void setMessage(String s){
      message = s;
   }
   
   public Player getSender(){
      return sender;
   }
   
   public void setSender(Player p){
      sender = p;
   }
   
   public Player getTarget(){
      return target;
   }
   
   public void setTarget(Player p){
      target = p;
   }
   
   public boolean isPrivateMessage(){
      return privateMessage;
   }
   
   public void setPrivateMessage(boolean b){
      privateMessage = b;
   }
}
