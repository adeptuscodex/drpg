/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Travis
 */
public class Player implements java.io.Serializable{
    
   boolean isGM = false;
   String playerName;
   int playerID;
   String charName;
   ArrayList<Token> tokens = new ArrayList();
   int hostID;
   String playerIP;
   ArrayList<String> diceMacros = new ArrayList();
   ArrayList<String> macroNames = new ArrayList();

   @Override
   public boolean equals(Object o){
      if(o instanceof Player)
      {
         Player p = (Player)o;
         if(p.getCharName().equals(this.getCharName())){
            return true;
         }
      }
      return false;
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 61 * hash + (this.isGM ? 1 : 0);
      hash = 61 * hash + Objects.hashCode(this.playerName);
      hash = 61 * hash + this.playerID;
      hash = 61 * hash + Objects.hashCode(this.charName);
      hash = 61 * hash + Objects.hashCode(this.tokens);
      hash = 61 * hash + this.hostID;
      hash = 61 * hash + Objects.hashCode(this.playerIP);
      hash = 61 * hash + Objects.hashCode(this.diceMacros);
      hash = 61 * hash + Objects.hashCode(this.macroNames);
      return hash;
   }
   
   public void setPlayerName(String input){
       playerName = input;
       
      }
   
   public String getPlayerName(){
       
       return playerName;
   }
    
    public void setIsGM(boolean input){
        isGM = input;
        
    }
    
    public boolean getIsGM(){
    
    
    return isGM;
    }
    
    public void setCharName(String input){
        charName = input;
        
    }
    
    public String getCharName(){
        
        return charName;
    }
    
    public ArrayList getTokens(){
        
        return tokens;
    }
    
    public Token getToken(int i){
        
        return tokens.get(i);
    }
    
    public void addToken(Token input){
        tokens.add(input);
        
        
    }
    
    public void removeToken(Token input){
        tokens.remove(input);
        
        
    }
    public void removeToken(ArrayList input){
        tokens.removeAll(input);
        
        
    }
    
    public void removeToken(int input){
        tokens.remove(input);
        
        
    }
    
    public void addToken(ArrayList input){
        tokens.addAll(input);
        
    }
    
    public void testList() {
    //testing line
   diceMacros.add("Success");
}
    
}