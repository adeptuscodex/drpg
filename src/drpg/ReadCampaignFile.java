/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;


import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.File;
import javax.swing.JOptionPane;

/**
 *
 * @author Adeptus Codex
 */
public class ReadCampaignFile {

    private ObjectInputStream input;

    public void openFile(File file) {
        try {

            input = new ObjectInputStream(new FileInputStream(file));
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Unable to open the File.", "File Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    public Campaign readData() {
        // create empty class incase of error
        Campaign empty = new Campaign();
        Campaign data;
        try {
            data = (Campaign) input.readObject();
            return data;
        } catch (ClassNotFoundException classNotFoundException) {
            JOptionPane.showMessageDialog(null, "Unable to create object.", "File Error", JOptionPane.ERROR_MESSAGE);
            return empty;
        }
        catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Error reading file.", "File Error", JOptionPane.ERROR_MESSAGE);
            return empty;
            
        }
    }
     public void closeFile(){
         try {
             if (input != null)
                 input.close();
         }
         catch (IOException ioException)
         {
             JOptionPane.showMessageDialog(null, "Error Closing file.", "File Error", JOptionPane.ERROR_MESSAGE);
         }
     }
    }

