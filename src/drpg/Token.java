/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;
import java.util.ArrayList;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;



/**
 *
 * @author Adeptus Codex
 */
public class Token extends JLabel implements java.io.Serializable{
    
    ArrayList<String> tags;
    ArrayList<String> GMtags;
    ImageIcon imageIcon;
    int gridType;
    long tokenID;
    String tokenName;
    Player owner;
    
    public Token(Icon i){
        super(i);
        tags = new ArrayList();
    }
    
    public void setImageIcon(ImageIcon image){
        imageIcon = image;
    }
    
    public ImageIcon getImageIcon(){
        
        return imageIcon;
    }
    public ArrayList getGMTags(){
        
        return tags;
    }
    
    public void addGMTag(String input){
        GMtags.add(input);
        
        
    }
    
    public void removeGMTag(String input){
        GMtags.remove(input);
        
    }
    
    public void editGMTag(String inputstring, String inputtag){
        if (GMtags.contains(inputtag) == true){
            GMtags.set(GMtags.lastIndexOf(inputtag), inputstring);
        }
        
        
    }
    
    public ArrayList getTags(){
        
        return tags;
    }
    
    public String getTag(int i){
        
        return tags.get(i);
    }
    
    public void addTag(String input){
        tags.add(input);
        
        
    }
    
    public void removeTag(String input){
        tags.remove(input);
        
    }
    
    public void editTag(String inputstring, String inputtag){
        if (tags.contains(inputtag) == true){
            tags.set(tags.lastIndexOf(inputtag), inputstring);
        }
        
        
    }
    
    public long getTokenID(){
        
        
        return tokenID;
    }
    
    public void setTokenID(long input){
        
        tokenID = input;
        
    }
    
    public int getGridType(){
        
        
        return gridType;
    }
    
    public void setGridType(int input){
        
        gridType = input;
        
        
    }
    
    public String getTokenName(){
        
        return tokenName;
    }
    
    public void setTokenName(String input){
        
        tokenName = input;
        
    }
    
    
    
}
