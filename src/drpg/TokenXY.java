/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Adeptus Codex
 */
public class TokenXY extends JLabel implements java.io.Serializable{
    
    private long tokenID;
    private Map parentMap;

    public TokenXY(ImageIcon i, long tokenID, Map parentMap){
       super(i);
       this.tokenID = tokenID;
       this.parentMap = parentMap;
    }
    
    public void setTokenID(long input){
       tokenID = input;
    }    
    
    public long getTokenID(){
       return tokenID;
    }
    
    public void setParentMap(Map m){
       parentMap = m;
    }
    
    public Map getParentMap(){
       return parentMap;
    }
}
