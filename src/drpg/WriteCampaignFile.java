/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package drpg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.NoSuchElementException;
import javax.swing.JOptionPane;

/**
 *
 * @author Adeptus Codex
 */
public class WriteCampaignFile {
    private ObjectOutputStream output;
    
     public void createFile(File file) {
        try {
            output = new ObjectOutputStream(new FileOutputStream(file+".cmp", false));
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Unable to create the File.", "File Error", JOptionPane.ERROR_MESSAGE);

        }
    }
         public void openFile(File file) {
        try {

            output = new ObjectOutputStream(new FileOutputStream(file));
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Unable to find the File.", "File Error", JOptionPane.ERROR_MESSAGE);

        }
         }    

   public void writeData(Campaign input){
        try {
            output.writeObject(input);
        } catch (NoSuchElementException elementException) {
            JOptionPane.showMessageDialog(null, "Invalid Input.", "Input Error", JOptionPane.ERROR_MESSAGE);
        }
        catch (IOException ioException) {
            JOptionPane.showMessageDialog(null, "Error writing file.", "File Error", JOptionPane.ERROR_MESSAGE);
            
        }
    }
     public void closeFile(){
         try {
             if (output != null)
                 output.close();
         }
         catch (IOException ioException)
         {
             JOptionPane.showMessageDialog(null, "Error Closing file.", "File Error", JOptionPane.ERROR_MESSAGE);
         }
     }
}